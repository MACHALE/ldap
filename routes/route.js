var express = require("express");
var ldap = require("ldapjs");

var router = express.Router();

module.exports = function (config) {
    
    function handleError(err, res, req) {
        return res.status(500).send(err || "Error");
    }
   
    var client = ldap.createClient({
        url:'ldap://34.244.98.51'
      });
     
    router.get('/login/:username/:password/', (req, res) => {                 // To get the document with specific ID
        var username = req.params.username;
        var password = req.params.password;
         var dn = "";
      var  details = {
            url: 'ldap://34.244.98.51',
            dn: 'cn=admin,dc=hygieia-virtusa,dc=org',
            pwd: '1234qwer$',
            suffix: 'dc=hygieia-virtusa,dc=org'
        }
        
       
          
        client.bind(details.dn, details.pwd, function(err) {

            
            if (err != null)
                res.send("Error: " + err);
            else
            
       
                // Search for a user with the correct UID.
            client.search(details.suffix, {
                    scope: "sub",
                    filter: "(uid=" + username + ")"
                }, function(err, ldapResult) {
                    if (err != null)
                        throw err;
                    else {
                        // If we get a result, then there is such a user.
                        ldapResult.on('searchEntry', function(entry) {
                            dn = entry.dn;
                            name = entry.object.cn;
                            
                            // When you have the DN, try to bind with it to check the password
                            var userClient = ldap.createClient({
                                url: details.url
                            });
                            userClient.bind(details.dn,password ,function(err) {
                                if (err == null) {
                                   
                                    res.send("blank page");
                                } else
                                    res.send("Invalid Credentials ");
                            });
                        });


                        
                        // If we get to the end and there is no DN, it means there is no such user.
                        ldapResult.on("end", function() {
                            if ( dn=== "")
                                res.send("No such user " + username); 
                        });
                    }
    
                }); 
                
        });
    });


    return router;
};
