var express = require("express");
var config = require("./config");
var router = require("./routes/route");
var bodyParser = require("body-parser");
var ldap = require("ldapjs");
var server = express();
 
// support json encoded bodies

//"D:/upload_data/filename3"

server.use(bodyParser.json());
// support encoded bodies
server.use(bodyParser.urlencoded({ extended: true }));

server.use(config.ROUTE_VIRTUAL_DIR + "/", router(config));

//start the server
server.listen(config.SERVER_PORT, function () {
    console.log("service started: " + config.SERVER_PORT);
});